package com.oraclechain.modbus;

import com.serotonin.modbus4j.ModbusMaster;

import java.net.InetAddress;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/12/2
 */
public class SiemensTestMain {

    public static final String ip = "192.168.2.3";

    public static void main(String[] args) throws Exception {
        Integer address = 0;
        Integer index = 0;
        Integer time = 1;
        Integer type = 1;
        Integer offset = 0;
        address = Integer.parseInt(args[0]);
        index = Integer.parseInt(args[1]);
        time = Integer.parseInt(args[2]);
        type = Integer.parseInt(args[3]);
        ModbusMaster master = TcpMaster.getMaster(ip);
        offset = address * time + index;
        Modbus4jReadUtils modbus4jReadUtils = new Modbus4jReadUtils();
        modbus4jReadUtils.batchRead(master, 1, offset, type, 8);
    }
}
