package com.oraclechain.modbus;

import com.serotonin.modbus4j.ModbusFactory;
import com.serotonin.modbus4j.ModbusMaster;
import com.serotonin.modbus4j.exception.ModbusInitException;
import com.serotonin.modbus4j.ip.IpParameters;
import com.serotonin.modbus4j.serial.SerialPortWrapper;

import java.io.InputStream;
import java.io.OutputStream;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/7/19
 */
public class TcpMaster {

    private static ModbusFactory modbusFactory;

    private static Map<String, ModbusMaster> map = new ConcurrentHashMap<>();

    static {
        if (modbusFactory == null) {
            modbusFactory = new ModbusFactory();
        }
    }

    /**
     * 获取master
     *
     * @return master
     */
    public static ModbusMaster getMaster(String key) {
        if (map.containsKey(key)) {
            return map.get(key);
        }
        IpParameters params = new IpParameters();
        params.setHost(key);
        params.setPort(502);
        ModbusMaster master = modbusFactory.createTcpMaster(params, false);// TCP 协议
        try {
            //设置超时时间
            master.setTimeout(1000);
            //设置重连次数
            master.setRetries(3);
            //初始化
            master.init();
            map.put(key, master);
            return master;
        } catch (ModbusInitException e) {
            e.printStackTrace();
            master.destroy();
            return null;
        }
    }

    public static ModbusMaster getMaster(String key, Integer port) {
        if (map.containsKey(key)) {
            return map.get(key);
        }
        IpParameters params = new IpParameters();
        params.setHost(key);
        params.setPort(port);
        ModbusMaster master = modbusFactory.createTcpMaster(params, false);// TCP 协议
        try {
            //设置超时时间
            master.setTimeout(1000);
            //设置重连次数
            master.setRetries(3);
            //初始化
            master.init();
            map.put(key, master);
            return master;
        } catch (ModbusInitException e) {
            e.printStackTrace();
            master.destroy();
            return null;
        }
    }

    public static ModbusMaster getRtuIpMaster(String host, int port) throws ModbusInitException {
        IpParameters params = new IpParameters();
        params.setHost(host);
        params.setPort(port);
        //这个属性确定了协议帧是否是通过tcp封装的RTU结构，采用modbus tcp/ip时，要设为false, 采用modbus rtu over tcp/ip时，要设为true
        params.setEncapsulated(true);
        ModbusMaster master = modbusFactory.createTcpMaster(params, false);
        try {
            //设置超时时间
            master.setTimeout(1000);
            //设置重连次数
            master.setRetries(3);
            //初始化
            master.init();
        } catch (ModbusInitException e) {
            e.printStackTrace();
        }
        return master;
    }

    public static ModbusMaster getRtuComMaster(String com,int port) throws ModbusInitException {
        //
        // modbusFactory.createRtuMaster(wapper); //RTU 协议
        // modbusFactory.createUdpMaster(params);//UDP 协议
        // modbusFactory.createAsciiMaster(wrapper);//ASCII 协议
        ModbusMaster master = modbusFactory.createRtuMaster(new SerialPortWrapperImpl(com,port));
        master.setTimeout(2000);

        master.init();

        return master;
    }

}
