package com.oraclechain.modbus;

import com.serotonin.modbus4j.ModbusMaster;
import com.serotonin.modbus4j.base.ModbusUtils;
import com.serotonin.modbus4j.code.DataType;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/7/19
 */
public class TestMain {

    public static void testTCP() throws Exception {
        ModbusMaster master = TcpMaster.getMaster("10.0.6.109");
        Modbus4jReadUtils modbus4jReadUtils = new Modbus4jReadUtils();
//        modbus4jReadUtils.batchRead(master);
        Modbus4jWriteUtils modbus4jWriteUtils = new Modbus4jWriteUtils();
        modbus4jWriteUtils.writeCoil(master, 1, 360, false);
        modbus4jWriteUtils.writeHoldingRegister(master, 2, 500, 1, DataType.FOUR_BYTE_FLOAT);
    }

    public static void testRTU() throws Exception {
        ModbusMaster master = TcpMaster.getRtuIpMaster("10.0.6.53",8234);
        Modbus4jReadUtils modbus4jReadUtils = new Modbus4jReadUtils();
        System.out.println(modbus4jReadUtils.readHoldingRegister(master,1,14, DataType.TWO_BYTE_INT_SIGNED));

    }

    public static void main(String[] args) throws Exception {
        testTCP();
//        testRTU();
    }

}
