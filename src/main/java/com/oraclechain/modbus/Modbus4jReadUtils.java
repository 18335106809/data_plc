package com.oraclechain.modbus;

import com.oraclechain.entity.DetailPLCInfo;
import com.oraclechain.entity.Device;
import com.serotonin.modbus4j.BatchRead;
import com.serotonin.modbus4j.BatchResults;
import com.serotonin.modbus4j.ModbusMaster;
import com.serotonin.modbus4j.code.DataType;
import com.serotonin.modbus4j.exception.ErrorResponseException;
import com.serotonin.modbus4j.exception.ModbusInitException;
import com.serotonin.modbus4j.exception.ModbusTransportException;
import com.serotonin.modbus4j.locator.BaseLocator;
import com.serotonin.modbus4j.msg.*;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/7/19
 */

@Component
public class Modbus4jReadUtils {

    /**
     * 读（线圈）开关量数据
     *
     * @param slaveId slaveId
     * @param offset  位置
     * @return 读取值
     */
    public boolean[] readCoilStatus(ModbusMaster master, int slaveId, int offset, int numberOfBits)
            throws ModbusTransportException, ErrorResponseException, ModbusInitException {

        ReadCoilsRequest request = new ReadCoilsRequest(slaveId, offset, numberOfBits);
        ReadCoilsResponse response = (ReadCoilsResponse) master.send(request);
        boolean[] booleans = response.getBooleanData();
        return valueRegroup(numberOfBits, booleans);
    }

    /**
     * 开关数据 读取外围设备输入的开关量
     */
    public boolean[] readInputStatus(ModbusMaster master, int slaveId, int offset, int numberOfBits)
            throws ModbusTransportException, ErrorResponseException, ModbusInitException {
        ReadDiscreteInputsRequest request = new ReadDiscreteInputsRequest(slaveId, offset, numberOfBits);
        ReadDiscreteInputsResponse response = (ReadDiscreteInputsResponse) master.send(request);
        boolean[] booleans = response.getBooleanData();
        return valueRegroup(numberOfBits, booleans);
    }

    /**
     * 读取保持寄存器数据
     *
     * @param slaveId slave Id
     * @param offset  位置
     */
    public short[] readHoldingRegister(ModbusMaster master, int slaveId, int offset, int numberOfBits)
            throws ModbusTransportException, ErrorResponseException, ModbusInitException {
        ReadHoldingRegistersRequest request = new ReadHoldingRegistersRequest(slaveId, offset, numberOfBits);
        ReadHoldingRegistersResponse response = (ReadHoldingRegistersResponse) master.send(request);
        return response.getShortData();
    }

    /**
     * 读取外围设备输入的数据
     *
     * @param slaveId slaveId
     * @param offset  位置
     */
    public short[] readInputRegisters(ModbusMaster master, int slaveId, int offset, int numberOfBits)
            throws ModbusTransportException, ErrorResponseException, ModbusInitException {

        ReadInputRegistersRequest request = new ReadInputRegistersRequest(slaveId, offset, numberOfBits);
        ReadInputRegistersResponse response = (ReadInputRegistersResponse) master.send(request);
        return response.getShortData();
    }

    /**
     * 批量读取 可以批量读取不同寄存器中数据
     */
    public BatchResults<Object> batchRead(ModbusMaster master, Integer slaveId, Integer offset, Integer type, Integer numType) throws ModbusTransportException, ErrorResponseException, ModbusInitException {
        BatchRead<Object> batch = new BatchRead<Object>();
        if (type.equals(1)) {
            batch.addLocator("1", BaseLocator.coilStatus(slaveId, offset));
        } else if (type.equals(2)) {
            batch.addLocator("1", BaseLocator.inputStatus(slaveId, offset));
        } else if (type.equals(3)) {
            batch.addLocator("1", BaseLocator.holdingRegister(slaveId, offset, numType));
        } else if (type.equals(4)) {
            batch.addLocator("1", BaseLocator.inputRegister(slaveId, offset, numType));
        }
        batch.setContiguousRequests(true);
        BatchResults<Object> results = master.send(batch);
        return results;
    }

    /**
     * 批量读取 可以批量读取不同寄存器中数据
     */
    public void batchRead(ModbusMaster master) throws ModbusTransportException, ErrorResponseException, ModbusInitException {

        BatchRead<Object> batch = new BatchRead<Object>();
//        batch.addLocator("wendu1", BaseLocator.holdingRegister(1, 60, DataType.FOUR_BYTE_FLOAT));
//        batch.addLocator("shidu1", BaseLocator.holdingRegister(1, 62, DataType.FOUR_BYTE_FLOAT));
        batch.addLocator("aaa", BaseLocator.holdingRegister(1, 64, DataType.FOUR_BYTE_FLOAT));
//        batch.addLocator("ccc", BaseLocator.holdingRegister(1, 66, DataType.FOUR_BYTE_FLOAT));
//        batch.addLocator("ddd", BaseLocator.holdingRegister(1, 14, DataType.FOUR_BYTE_FLOAT));
//        batch.addLocator("y", BaseLocator.holdingRegister(1, 464, DataType.FOUR_BYTE_FLOAT));
//        batch.addLocator("w", BaseLocator.holdingRegister(1, 472, DataType.FOUR_BYTE_FLOAT));
//        batch.addLocator("j", BaseLocator.holdingRegister(1, 577, DataType.FOUR_BYTE_FLOAT));
//        batch.addLocator("fan", BaseLocator.coilStatus(1, 4016));
//        batch.addLocator(1, BaseLocator.coilStatus(1, 0));
//        batch.addLocator("3", BaseLocator.holdingRegister(1, 4, DataType.EIGHT_BYTE_FLOAT));
        batch.addLocator("1", BaseLocator.coilStatus(1, 353));
        batch.addLocator("2", BaseLocator.coilStatus(1, 355));
        batch.addLocator("3", BaseLocator.coilStatus(1, 356));
        batch.addLocator("4", BaseLocator.coilStatus(1, 361));
        batch.addLocator("5", BaseLocator.coilStatus(1, 363));
        batch.addLocator("6", BaseLocator.coilStatus(1, 364));
        batch.setContiguousRequests(true);
        BatchResults<Object> results = master.send(batch);
//        System.out.println("batchRead:" + results.getValue("wendu1"));
//        System.out.println("batchRead:" + results.getValue("shidu1"));
//        System.out.println("batchRead:" + results.getValue("aaa"));
//        System.out.println("batchRead:" + results.getValue("ccc"));
//        System.out.println("batchRead:" + results.getValue("ddd"));
        System.out.println("batchRead:" + results.getValue("1"));
        System.out.println("batchRead:" + results.getValue("2"));
        System.out.println("batchRead:" + results.getValue("3"));
        System.out.println("batchRead:" + results.getValue("4"));
        System.out.println("batchRead:" + results.getValue("5"));
        System.out.println("batchRead:" + results.getValue("6"));
    }

    /**
     * 批量读取 可以批量读取不同寄存器中数据
     */
    public BatchResults<Object> batchRead(ModbusMaster master, List<DetailPLCInfo> detailPLCInfoList) throws ModbusTransportException, ErrorResponseException, ModbusInitException {
        BatchRead<Object> batch = new BatchRead<>();
        for (DetailPLCInfo plcInfo : detailPLCInfoList) {
            if (plcInfo.getIsRead().equals(1)) {
                if (plcInfo.getReadType().equals(3)) {
                    batch.addLocator(plcInfo.getDeviceId() + plcInfo.getPlcAddress(), BaseLocator.holdingRegister(1, plcInfo.getOffset(), DataType.FOUR_BYTE_FLOAT));
                } else if (plcInfo.getReadType().equals(1)) {
                    batch.addLocator(plcInfo.getDeviceId() + plcInfo.getPlcAddress(), BaseLocator.coilStatus(1, plcInfo.getOffset()));
                } else if (plcInfo.getReadType().equals(2)) {
                    batch.addLocator(plcInfo.getDeviceId() + plcInfo.getPlcAddress(), BaseLocator.inputStatus(1, plcInfo.getOffset()));
                }
            }
        }
        batch.setContiguousRequests(true);
        try {
            BatchResults<Object> results = master.send(batch);
            return results;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private boolean[] valueRegroup(int numberOfBits, boolean[] values) {
        boolean[] bs = new boolean[numberOfBits];
        int temp = 1;
        for (boolean b : values) {
            bs[temp - 1] = b;
            temp++;
            if (temp > numberOfBits)
                break;
        }
        return bs;
    }

}
