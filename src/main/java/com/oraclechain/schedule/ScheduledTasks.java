package com.oraclechain.schedule;

import com.alibaba.fastjson.JSONObject;
import com.oraclechain.config.RedisKeyConfig;
import com.oraclechain.config.StringToArrayUtil;
import com.oraclechain.entity.DetailPLCInfo;
import com.oraclechain.entity.Device;
import com.oraclechain.entity.DeviceData;
import com.oraclechain.modbus.Modbus4jReadUtils;
import com.oraclechain.modbus.TcpMaster;
import com.oraclechain.mq.TopicRabbitConfig;
import com.serotonin.modbus4j.BatchResults;
import io.netty.util.internal.StringUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author liuhualong
 * @Description: 设备健康状态定时监测
 * @date 2020/4/8
 */
@Component
@EnableScheduling
public class ScheduledTasks {

    static Logger logger = LoggerFactory.getLogger(ScheduledTasks.class);

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Autowired
    private Modbus4jReadUtils modbus4jReadUtils;

    @Autowired
    private RedisTemplate<String, String> redisTemplate;

    private static Map<String, Map<Long, List<DetailPLCInfo>>> specialDeviceMap = new ConcurrentHashMap<>();

    private static Map<String, Map<Long, List<DetailPLCInfo>>> deviceMap = new ConcurrentHashMap<>();

    public static void setSpecialDeviceMap(Map<String, Map<Long, List<DetailPLCInfo>>> map) {
        specialDeviceMap = map;
    }

    public static void setDeviceMap(Map<String, Map<Long, List<DetailPLCInfo>>> map) {
        deviceMap = map;
    }

//    @Scheduled(cron = "0/20 * * * * *")
    public void mockData() {
        logger.info("执行ModbusUtil, 生产数据定时任务>>>>>>>>>>>>>>>>>");
        readAndSend(deviceMap, false, false);
    }

//    @Scheduled(cron = "0/1 * * * * *")
    public void mockSpecialData1() {
        try {
            readAndSend(specialDeviceMap, true, true);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("读取電艙特殊设备数据失败");
        }
    }

    //    @Scheduled(cron = "0/1 * * * * *")
    public void mockSpecialData2() {
        try {
            readAndSend(specialDeviceMap, true, false);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("读取水艙特殊设备数据失败");
        }
    }

    public void readAndSend(Map<String, Map<Long, List<DetailPLCInfo>>> map, Boolean isSpecial, Boolean isElectric) {
        try {
            for(Map.Entry<String, Map<Long, List<DetailPLCInfo>>> entry : map.entrySet()){
                String mapKey = entry.getKey();
                if (isSpecial) {
                    if (isElectric && dealIp(mapKey) >= 200) {
                        continue;
                    } else if (!isElectric && dealIp(mapKey) < 200) {
                        continue;
                    }
                }
                Map<Long, List<DetailPLCInfo>> mapValue = entry.getValue();
                List<DetailPLCInfo> plcInfoList = new ArrayList<>();
                for (Map.Entry<Long, List<DetailPLCInfo>> entry1 : mapValue.entrySet()) {
                    plcInfoList.addAll(entry1.getValue());
                }
                BatchResults<Object> values = modbus4jReadUtils.batchRead(TcpMaster.getMaster(mapKey), plcInfoList);
                if (values == null) {
                    continue;
                }
                for (Long deviceId : mapValue.keySet()) {
                    List<DetailPLCInfo> detailPLCInfoList = mapValue.get(deviceId);
                    Device device = getDevice(deviceId);
                    DeviceData deviceData = new DeviceData();
                    deviceData.setId(deviceId);
                    JSONObject jsonObject = new JSONObject();
                    if (device == null) {
                        continue;
                    }
                    List<String> list = StringToArrayUtil.toArray(device.getParameters());
                    for (DetailPLCInfo detailPLCInfo : detailPLCInfoList) {
                        for (String param : list) {
                            if (!StringUtils.isEmpty(detailPLCInfo.getDataKey()) && detailPLCInfo.getDataKey().equals(param)) {
                                if (detailPLCInfo.getReadType().equals(1) || detailPLCInfo.getReadType().equals(2)) {
                                    jsonObject.put(param, Boolean.valueOf(values.getValue(device.getId() + detailPLCInfo.getPlcAddress()).toString()) ? 1 : 0);
                                } else if (detailPLCInfo.getReadType().equals(3)) {
                                    jsonObject.put(param, values.getValue(device.getId() + detailPLCInfo.getPlcAddress()));
                                }
                            }
                        }
                    }
                    deviceData.setParam(jsonObject);
                    if (isSpecial) {
                        this.rabbitTemplate.convertAndSend(TopicRabbitConfig.EXCHANGE_SPECIAL, TopicRabbitConfig.DEVICE_SPECIAL, JSONObject.toJSONString(deviceData));
                    } else {
                        this.rabbitTemplate.convertAndSend(TopicRabbitConfig.EXCHANGE, TopicRabbitConfig.DEVICE, JSONObject.toJSONString(deviceData));
                    }
                }
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
            e.printStackTrace();
        }
    }

    public Integer dealIp(String ip) {
        if (ip.contains(".")) {
            ip = ip.substring(ip.lastIndexOf(".") + 1);
            return Integer.valueOf(ip);
        } else {
            return 0;
        }
    }

    public Device getDevice(Long deviceId) {
        String value = redisTemplate.opsForValue().get(RedisKeyConfig.DEVICE_KEY + deviceId);
        if (StringUtils.isEmpty(value)) {
            return null;
        }
        Device device = JSONObject.parseObject(value, Device.class);
        return device;
    }

}
