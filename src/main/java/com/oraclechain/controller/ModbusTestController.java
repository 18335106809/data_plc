package com.oraclechain.controller;

import com.alibaba.fastjson.JSONObject;
import com.oraclechain.entity.ModbusAddress;
import com.oraclechain.modbus.Modbus4jReadUtils;
import com.oraclechain.modbus.Modbus4jWriteUtils;
import com.oraclechain.modbus.TcpMaster;
import com.oraclechain.util.ResultUtil;
import com.serotonin.modbus4j.BatchResults;
import com.serotonin.modbus4j.ModbusMaster;
import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/12/3
 */
@RestController
@RequestMapping("/modbus")
@Api(description = "plc信息相关操作")
public class ModbusTestController {

    static Logger logger = LoggerFactory.getLogger(ModbusTestController.class);

    @RequestMapping(value = "testAddress", method = RequestMethod.POST)
    @ResponseBody
    public String testAddress(@RequestBody ModbusAddress modbusAddress) {
        logger.info("测试modbus地址");
        try {
            ModbusMaster master = TcpMaster.getMaster(modbusAddress.getIp(), modbusAddress.getPort());
            Integer offset = modbusAddress.getAddress() * modbusAddress.getTime() + modbusAddress.getIndex();
            if (modbusAddress.getIsRead().equals(1)) {
                Modbus4jReadUtils modbus4jReadUtils = new Modbus4jReadUtils();
                BatchResults<Object> results = modbus4jReadUtils.batchRead(master, modbusAddress.getSlaveId(), offset, modbusAddress.getType(), modbusAddress.getNumType());
                return JSONObject.toJSONString(ResultUtil.success(results.getValue("1")));
            } else {
                Modbus4jWriteUtils modbus4jWriteUtils = new Modbus4jWriteUtils();
                if (modbusAddress.getType().equals(1)) {
                    modbus4jWriteUtils.writeCoil(master, modbusAddress.getSlaveId(), offset, modbusAddress.getValue().equals(1) ? true : false);
                } else if (modbusAddress.getType().equals(3)) {
                    modbus4jWriteUtils.writeHoldingRegister(master, modbusAddress.getSlaveId(), offset, modbusAddress.getValue(), modbusAddress.getNumType());
                }
                return JSONObject.toJSONString(ResultUtil.success());
            }
        } catch (Exception e) {
            e.printStackTrace();
            return JSONObject.toJSONString(ResultUtil.error("测试modbus失败"));
        }
    }

}
