package com.oraclechain.controller;

import com.alibaba.fastjson.JSONObject;
import com.oraclechain.config.RedisKeyConfig;
import com.oraclechain.entity.DetailPLCInfo;
import com.oraclechain.entity.Device;
import com.oraclechain.schedule.ScheduledTasks;
import com.oraclechain.util.ResultUtil;
import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.RedisSystemException;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/11/15
 */
@RestController
@RequestMapping("/cache")
@Api(description = "plc信息相关操作")
public class DeviceCacheController {

    static Logger logger = LoggerFactory.getLogger(DeviceCacheController.class);

    @Autowired
    private RedisTemplate<String, String> redisTemplate;

    @Value("${device.special.type}")
    private String specialType;

    @RequestMapping(value = "updateSpecialCache", method = RequestMethod.POST)
    @ResponseBody
    public String updateSpecialDeviceCache() {
        logger.info("更新redis中的特殊设备类型的key");
        try {
            Map<String, Map<Long, List<DetailPLCInfo>>> result = new ConcurrentHashMap<>();
            try {
                Set<String> keys = redisTemplate.keys(RedisKeyConfig.DEVICE_PLC_IP_KEY + "*");
                for (String ipKey : keys) {
                    String ip = ipKey.substring(ipKey.indexOf(":") + 1);
                    Set<String> deviceIdSet = redisTemplate.opsForSet().members(ipKey);
                    Map<Long, List<DetailPLCInfo>> plcInfoMap = new ConcurrentHashMap<>();
                    for (String deviceId : deviceIdSet) {
                        Device device = getDevice(deviceId);
                        if (device != null && !StringUtils.isEmpty(device.getType()) && specialType.contains(device.getType())) {
                            List<DetailPLCInfo> plcInfoList = new ArrayList<>();
                            Set<Object> plcAddressSet = redisTemplate.opsForHash().keys(RedisKeyConfig.DEVICE_PLC_INFO + deviceId);
                            List<Object> values = redisTemplate.opsForHash().multiGet(RedisKeyConfig.DEVICE_PLC_INFO + deviceId, plcAddressSet);
                            for (Object plcIno : values) {
                                DetailPLCInfo detailPLCInfo = JSONObject.parseObject(plcIno.toString(), DetailPLCInfo.class);
                                if (detailPLCInfo.getIsRead().equals(1) || detailPLCInfo.getIsRead().equals(2)) {
                                    plcInfoList.add(detailPLCInfo);
                                }
                            }
                            plcInfoMap.put(Long.parseLong(deviceId), plcInfoList);
                        }
                    }
                    result.put(ip, plcInfoMap);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            ScheduledTasks.setSpecialDeviceMap(result);

            Set<String> keys = redisTemplate.keys(RedisKeyConfig.DEVICE_KEY + "*");
            for (String key : keys) {
                Device device = getDevice(key);
                if (device != null && !StringUtils.isEmpty(device.getType()) && specialType.contains(device.getType())) {
                    redisTemplate.opsForSet().add(RedisKeyConfig.DEVICE_SPECIAL_TYPE, device.getId().toString());
                }
            }
            return JSONObject.toJSONString(ResultUtil.success());
        } catch (Exception e) {
            e.printStackTrace();
            return JSONObject.toJSONString(ResultUtil.error("更新redis失败"));
        }
    }

    @RequestMapping(value = "updateCache", method = RequestMethod.POST)
    @ResponseBody
    public String updateDeviceCache() {
        logger.info("更新redis中的设备的key");
        try {
            Map<String, Map<Long, List<DetailPLCInfo>>> result = new ConcurrentHashMap<>();
            try {
                Set<String> keys = redisTemplate.keys(RedisKeyConfig.DEVICE_PLC_IP_KEY + "*");
                for (String ipKey : keys) {
                    String ip = ipKey.substring(ipKey.indexOf(":") + 1);
                    Set<String> deviceIdSet = redisTemplate.opsForSet().members(ipKey);
                    Map<Long, List<DetailPLCInfo>> plcInfoMap = new ConcurrentHashMap<>();
                    for (String deviceId : deviceIdSet) {
                        List<DetailPLCInfo> plcInfoList = new ArrayList<>();
                        Set<Object> plcAddressSet = redisTemplate.opsForHash().keys(RedisKeyConfig.DEVICE_PLC_INFO + deviceId);
                        List<Object> values = redisTemplate.opsForHash().multiGet(RedisKeyConfig.DEVICE_PLC_INFO + deviceId, plcAddressSet);
                        for (Object plcIno : values) {
                            DetailPLCInfo detailPLCInfo = JSONObject.parseObject(plcIno.toString(), DetailPLCInfo.class);
                            if (detailPLCInfo.getIsRead().equals(1) || detailPLCInfo.getIsRead().equals(2)) {
                                plcInfoList.add(detailPLCInfo);
                            }
                        }
                        plcInfoMap.put(Long.parseLong(deviceId), plcInfoList);
                    }
                    result.put(ip, plcInfoMap);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            ScheduledTasks.setDeviceMap(result);
            return JSONObject.toJSONString(ResultUtil.success());
        } catch (Exception e) {
            e.printStackTrace();
            return JSONObject.toJSONString(ResultUtil.error("更新redis失败"));
        }
    }



    public Device getDevice(String redisKey) {
        String value = redisTemplate.opsForValue().get(redisKey);
        if (StringUtils.isEmpty(value)) {
            return null;
        }
        Device device = JSONObject.parseObject(value, Device.class);
        return device;
    }
}
