package com.oraclechain.controller;

import com.alibaba.fastjson.JSONObject;
import com.oraclechain.entity.DetailPLCInfo;
import com.oraclechain.entity.PLCReadTypeEnum;
import com.oraclechain.entity.PLCWriteTypeEnum;
import com.oraclechain.service.DetailPLCInfoService;
import com.oraclechain.util.ResultUtil;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.sound.midi.Soundbank;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/10/12
 */
@RestController
@RequestMapping("/insert")
@Api(description = "生成相关操作")
public class InsertController {

    @Autowired
    private DetailPLCInfoService detailPLCInfoService;

    static Integer port = 502;

    @RequestMapping(value = "insert", method = RequestMethod.POST)
    @ResponseBody
    public String insert(@RequestParam(value="ip", required=false) String ip,
                         @RequestParam(value="readLine", required=false) Integer readLine,
                         @RequestParam(value="line", required=false) Integer line,
                         @RequestParam(value="deviceId", required=false) Long deviceId,
                         @RequestParam(value="isPulse", required=false) Integer isPulse,
                         @RequestParam(value="readType", required=false) Integer readType) {
        try {
            File file = new File("C:\\Users\\Think\\Desktop\\ggg.txt");
            List<String> dataList = new ArrayList<>();
            try (BufferedReader br = new BufferedReader(new FileReader(file))) {
                String lineData = null;
                while ((lineData = br.readLine()) != null) {
                    System.out.println(dataList.add(lineData.trim()));
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            List<List<String>> list = new ArrayList<>();
            List<String> deviceList = new ArrayList<>();
            for (Integer i = 1; i <= dataList.size(); i++) {
                deviceList.add(dataList.get(i - 1));
                if (i % line == 0) {
                    list.add(deviceList);
                    deviceList = new ArrayList<>();
                }
            }
            for (List<String> tmpList : list) {
                for (int i = 1; i <= tmpList.size(); i++) {
                    Integer isRead = 1;
                    String type = "";
                    String str = tmpList.get(i - 1);
                    String[] strs = str.split("\t");
                    String plcAddress = strs[0];
                    String desc = strs[1];
                    Integer offset = getOffSet(plcAddress);
                    if (i <= readLine) {
                        isRead = 0;
                        type = PLCWriteTypeEnum.getName(desc).toString();
                    } else {
                        if (plcAddress.contains("MD")) {
                            type = "VALUE";
                        } else {
                            type = PLCReadTypeEnum.getName(desc).toString();
                        }
                    }

                    String dataKey = null;
                    if (desc.equals("自动信号反馈")) {
                        dataKey = "auto";
                    } else if (desc.equals("低速运行")) {
                        dataKey = "low_open";
                    } else if (desc.equals("低速故障")) {
                        dataKey = "low_breakdown";
                    } else if (desc.equals("高速运行")) {
                        dataKey = "high_open";
                    } else if (desc.equals("高速故障")) {
                        dataKey = "high_breakdown";
                    } else if (desc.contains("运行")) {
                        dataKey = "open";
                    } else if (desc.contains("故障")) {
                        dataKey = "breakdown";
                    } else if (desc.contains("压力")) {
                        dataKey = "horizontal";
                    } else if (desc.contains("温度")) {
                        dataKey = "temperature";
                    } else if (desc.contains("湿度")) {
                        dataKey = "humidity";
                    } else if (desc.contains("氧气")) {
                        dataKey = "oxygen";
                    } else if (desc.contains("二氧化碳")) {
                        dataKey = "co2";
                    } else if (desc.contains("甲烷")) {
                        dataKey = "methane";
                    } else if (desc.contains("积水")) {
                        dataKey = "level";
                    }


                    DetailPLCInfo detailPLCInfo = new DetailPLCInfo();
                    detailPLCInfo.setIp(ip);
                    detailPLCInfo.setPort(port);
                    detailPLCInfo.setDeviceId(deviceId);
                    detailPLCInfo.setType(type);
                    detailPLCInfo.setPlcAddress(plcAddress);
                    detailPLCInfo.setOffset(offset);
                    detailPLCInfo.setIsRead(isRead);
                    detailPLCInfo.setIsPulse(isPulse);
                    detailPLCInfo.setDescription(desc);
                    detailPLCInfo.setReadType(readType);
                    detailPLCInfo.setDataKey(dataKey);
                    detailPLCInfoService.insert(detailPLCInfo);
                }
                deviceId += 1;
            }

            return JSONObject.toJSONString(ResultUtil.success());
        } catch (Exception e) {
            e.printStackTrace();
            return JSONObject.toJSONString(ResultUtil.error("更新redis失败"));
        }
    }

    public static Integer getOffSet(String plc) {
        Integer result = 0;
        if (plc.contains("MX")) {
            plc = plc.replaceAll("%MX", "");
            String[] strings = plc.split("\\.");
            Integer int1 = Integer.parseInt(strings[1]);
            Integer int2 = Integer.parseInt(strings[2]);
            result = (int1 * 8) + int2;
        } else if (plc.contains("MD")) {
            plc = plc.replaceAll("%MD", "");
            String[] strings = plc.split("\\.");
            Integer int1 = Integer.parseInt(strings[0]);
            Integer int2 = Integer.parseInt(strings[1]);
            result = (int2 * 2);
        }
        return result;
    }

    public static void main(String[] args) {
        System.out.println(getOffSet("%MX0.11.2"));
        System.out.println(getOffSet("%MD0.112"));
        System.out.println(PLCReadTypeEnum.getName("运行").toString());
    }
}
