package com.oraclechain.controller;

import com.alibaba.fastjson.JSONObject;
import com.oraclechain.entity.DetailPLCInfo;
import com.oraclechain.entity.PLCReadTypeEnum;
import com.oraclechain.entity.PLCWriteTypeEnum;
import com.oraclechain.service.DetailPLCInfoService;
import com.oraclechain.util.ResultUtil;
import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/9/22
 */
@RestController
@RequestMapping("/plcInfo")
@Api(description = "plc信息相关操作")
public class PLCInfoController {

    static Logger logger = LoggerFactory.getLogger(PLCInfoController.class);

    @Autowired
    private DetailPLCInfoService detailPLCInfoService;

    @RequestMapping(value = "updateCache", method = RequestMethod.POST)
    @ResponseBody
    public String updateCache() {
        logger.info("更新redis中的plcInfo信息");
        try {
            detailPLCInfoService.updateCache();
            return JSONObject.toJSONString(ResultUtil.success());
        } catch (Exception e) {
            e.printStackTrace();
            return JSONObject.toJSONString(ResultUtil.error("更新redis失败"));
        }
    }

    @RequestMapping(value = "select", method = RequestMethod.POST)
    @ResponseBody
    public String select(@RequestBody DetailPLCInfo detailPLCInfo) {
        logger.info("通过deviceId获取对应的plc信息");
        try {
            List<DetailPLCInfo> result = detailPLCInfoService.select(detailPLCInfo);
            return JSONObject.toJSONString(ResultUtil.success(result));
        } catch (Exception e) {
            e.printStackTrace();
            return JSONObject.toJSONString(ResultUtil.error("获取plcInfo失败"));
        }
    }

    @RequestMapping(value = "insert", method = RequestMethod.POST)
    @ResponseBody
    public String insert(@RequestBody DetailPLCInfo detailPLCInfo) {
        logger.info("新增plc信息");
        try {
            detailPLCInfoService.insert(detailPLCInfo);
            return JSONObject.toJSONString(ResultUtil.success());
        } catch (Exception e) {
            e.printStackTrace();
            return JSONObject.toJSONString(ResultUtil.error("新增plcInfo失败"));
        }
    }

    @RequestMapping(value = "update", method = RequestMethod.POST)
    @ResponseBody
    public String update(@RequestBody DetailPLCInfo detailPLCInfo) {
        logger.info("更新plc信息");
        try {
            detailPLCInfoService.update(detailPLCInfo);
            return JSONObject.toJSONString(ResultUtil.success());
        } catch (Exception e) {
            e.printStackTrace();
            return JSONObject.toJSONString(ResultUtil.error("更新plcInfo失败"));
        }
    }

    @RequestMapping(value = "delete", method = RequestMethod.POST)
    @ResponseBody
    public String delete(@RequestBody DetailPLCInfo detailPLCInfo) {
        logger.info("删除plc信息");
        try {
            detailPLCInfoService.delete(detailPLCInfo.getId());
            return JSONObject.toJSONString(ResultUtil.success());
        } catch (Exception e) {
            e.printStackTrace();
            return JSONObject.toJSONString(ResultUtil.error("删除plcInfo失败"));
        }
    }

    @RequestMapping(value = "getType", method = RequestMethod.POST)
    @ResponseBody
    public String getType() {
        logger.info("获取类型集合");
        try {
            JSONObject result = new JSONObject();
            result.put("read", PLCReadTypeEnum.getValues());
            result.put("write", PLCWriteTypeEnum.getValues());
            return JSONObject.toJSONString(ResultUtil.success(result));
        } catch (Exception e) {
            e.printStackTrace();
            return JSONObject.toJSONString(ResultUtil.error("获取plc信息失败"));
        }
    }
}
