package com.oraclechain.dao;

import com.oraclechain.entity.PLCInfo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/7/21
 */
@Mapper
public interface PLCInfoDao {
    void insert(PLCInfo plcInfo);
    void update(PLCInfo plcInfo);
    List<PLCInfo> select(PLCInfo plcInfo);
    void delete(Integer id);
}
