package com.oraclechain.dao;

import com.oraclechain.entity.DetailPLCInfo;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/7/21
 */
@Mapper
public interface DetailPLCInfoDao {
    void insert(DetailPLCInfo detailPLCInfo);
    void update(DetailPLCInfo detailPLCInfo);
    List<DetailPLCInfo> select(DetailPLCInfo detailPLCInfo);
    void delete(Integer id);
}
