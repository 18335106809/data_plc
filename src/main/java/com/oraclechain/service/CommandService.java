package com.oraclechain.service;

import com.oraclechain.entity.Device;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/9/22
 */
public interface CommandService {
    void sendOpenCommand(Device device);
    void sendCloseCommand(Device device);
}
