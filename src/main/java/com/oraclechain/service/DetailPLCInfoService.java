package com.oraclechain.service;

import com.oraclechain.dao.DetailPLCInfoDao;
import com.oraclechain.entity.DetailPLCInfo;

import java.util.List;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/7/24
 */
public interface DetailPLCInfoService {
    List<DetailPLCInfo> select(DetailPLCInfo detailPLCInfo);
    void insert(DetailPLCInfo detailPLCInfo);
    void update(DetailPLCInfo detailPLCInfo);
    void delete(Integer id);
    void updateCache();
}
