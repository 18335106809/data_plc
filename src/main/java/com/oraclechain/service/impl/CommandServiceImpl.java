package com.oraclechain.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.oraclechain.config.RedisKeyConfig;
import com.oraclechain.entity.CRC;
import com.oraclechain.entity.DetailPLCInfo;
import com.oraclechain.entity.Device;
import com.oraclechain.entity.PLCWriteTypeEnum;
import com.oraclechain.modbus.Modbus4jWriteUtils;
import com.oraclechain.modbus.TcpMaster;
import com.oraclechain.service.CommandService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/9/22
 */
@Service
public class CommandServiceImpl implements CommandService {

    static Logger logger = LoggerFactory.getLogger(CommandServiceImpl.class);

    @Autowired
    private RedisTemplate<String, String> redisTemplate;

    @Autowired
    private Modbus4jWriteUtils modbus4jWriteUtils;

    @Override
    public void sendOpenCommand(Device device) {
        logger.info("send device open command, deviceId : " + device.getId());
        try {
            sendCommand(device, true);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("send device open command failed");
        }
    }

    @Override
    public void sendCloseCommand(Device device) {
        logger.info("send device close command, deviceId : " + device.getId());
        try {
            sendCommand(device, false);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("send device close command failed");
        }
    }

    public void sendCommand(Device device, Boolean isOpen) {
        try {
            if (device.getType().equals("two_fan") && StringUtils.isEmpty(device.getOrderDataKey())) {
                device.setOrderDataKey("high_open");
            }
            List<Object> values = redisTemplate.opsForHash().values(RedisKeyConfig.DEVICE_PLC_INFO + device.getId());
            if (!StringUtils.isEmpty(device.getOrderDataKey())) {
                sendCommondWithType(device, values, isOpen);
            } else {
                for (Object temp : values) {
                    DetailPLCInfo detailPLCInfo = JSONObject.parseObject(temp.toString(), DetailPLCInfo.class);
                    // isRead == 0 表示为写 使用PLCWriteTypeEnum、PLCReadTypeEnum
                    if (detailPLCInfo.getIsRead().equals(0)) {
                        // isPulse == 1 表示为脉冲信号，需要两个指令控制
                        if (detailPLCInfo.getIsPulse().equals(1)) {
                            if (isOpen) {
                                if (detailPLCInfo.getType().equals(PLCWriteTypeEnum.OPEN.toString())) {
                                    modbus4jWriteUtils.writeCoil(TcpMaster.getMaster(detailPLCInfo.getIp()), 1, detailPLCInfo.getOffset(), true);
                                } else if (detailPLCInfo.getType().equals(PLCWriteTypeEnum.STOP.toString())) {
                                    modbus4jWriteUtils.writeCoil(TcpMaster.getMaster(detailPLCInfo.getIp()), 1, detailPLCInfo.getOffset(), false);
                                }
                            } else {
                                if (detailPLCInfo.getType().equals(PLCWriteTypeEnum.OPEN.toString())) {
                                    modbus4jWriteUtils.writeCoil(TcpMaster.getMaster(detailPLCInfo.getIp()), 1, detailPLCInfo.getOffset(), false);
                                } else if (detailPLCInfo.getType().equals(PLCWriteTypeEnum.STOP.toString())) {
                                    modbus4jWriteUtils.writeCoil(TcpMaster.getMaster(detailPLCInfo.getIp()), 1, detailPLCInfo.getOffset(), true);
                                }
                            }
                        } else {
                            if (isOpen) {
                                if (detailPLCInfo.getType().equals(PLCWriteTypeEnum.OPEN.toString())) {
                                    modbus4jWriteUtils.writeCoil(TcpMaster.getMaster(detailPLCInfo.getIp()), 1, detailPLCInfo.getOffset(), true);
                                }
                            } else {
                                if (detailPLCInfo.getType().equals(PLCWriteTypeEnum.OPEN.toString())) {
                                    modbus4jWriteUtils.writeCoil(TcpMaster.getMaster(detailPLCInfo.getIp()), 1, detailPLCInfo.getOffset(), false);
                                }
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // 如果是非auto信号，判断是否为脉冲，判断是否为其它open
    public void sendCommondWithType(Device device, List<Object> values, Boolean isOpen) {
        try {
            if (device.getOrderDataKey().equals("auto")) {
                for (Object temp : values) {
                    DetailPLCInfo detailPLCInfo = JSONObject.parseObject(temp.toString(), DetailPLCInfo.class);
                    if (detailPLCInfo.getIsRead().equals(0) && detailPLCInfo.getType().equals(PLCWriteTypeEnum.AUTO.toString())) {
                        if (isOpen) {
                            modbus4jWriteUtils.writeCoil(TcpMaster.getMaster(detailPLCInfo.getIp()), 1, detailPLCInfo.getOffset(), true);
                        } else {
                            modbus4jWriteUtils.writeCoil(TcpMaster.getMaster(detailPLCInfo.getIp()), 1, detailPLCInfo.getOffset(), false);
                        }
                    }
                }
            } else if (device.getOrderDataKey().equals("high_open")) {
                for (Object temp : values) {
                    DetailPLCInfo detailPLCInfo = JSONObject.parseObject(temp.toString(), DetailPLCInfo.class);
                    if (detailPLCInfo.getIsRead().equals(0)) {
                        if (detailPLCInfo.getIsPulse().equals(1)) {
                            if (isOpen) {
                                if (detailPLCInfo.getType().equals(PLCWriteTypeEnum.HIGH_SPEED_OPEN.toString())) {
                                    modbus4jWriteUtils.writeCoil(TcpMaster.getMaster(detailPLCInfo.getIp()), 1, detailPLCInfo.getOffset(), true);
                                } else if (detailPLCInfo.getType().equals(PLCWriteTypeEnum.HIGH_SPEED_STOP.toString())) {
                                    modbus4jWriteUtils.writeCoil(TcpMaster.getMaster(detailPLCInfo.getIp()), 1, detailPLCInfo.getOffset(), false);
                                }
                            } else {
                                if (detailPLCInfo.getType().equals(PLCWriteTypeEnum.HIGH_SPEED_OPEN.toString())) {
                                    modbus4jWriteUtils.writeCoil(TcpMaster.getMaster(detailPLCInfo.getIp()), 1, detailPLCInfo.getOffset(), false);
                                } else if (detailPLCInfo.getType().equals(PLCWriteTypeEnum.HIGH_SPEED_STOP.toString())) {
                                    modbus4jWriteUtils.writeCoil(TcpMaster.getMaster(detailPLCInfo.getIp()), 1, detailPLCInfo.getOffset(), true);
                                }
                            }
                        } else {
                            if (isOpen) {
                                if (detailPLCInfo.getType().equals(PLCWriteTypeEnum.HIGH_SPEED_OPEN.toString())) {
                                    modbus4jWriteUtils.writeCoil(TcpMaster.getMaster(detailPLCInfo.getIp()), 1, detailPLCInfo.getOffset(), true);
                                }
                            } else {
                                if (detailPLCInfo.getType().equals(PLCWriteTypeEnum.HIGH_SPEED_OPEN.toString())) {
                                    modbus4jWriteUtils.writeCoil(TcpMaster.getMaster(detailPLCInfo.getIp()), 1, detailPLCInfo.getOffset(), false);
                                }
                            }
                        }
                    }
                }
            } else if (device.getOrderDataKey().equals("low_open")) {
                for (Object temp : values) {
                    DetailPLCInfo detailPLCInfo = JSONObject.parseObject(temp.toString(), DetailPLCInfo.class);
                    if (detailPLCInfo.getIsRead().equals(0)) {
                        if (detailPLCInfo.getIsPulse().equals(1)) {
                            if (isOpen) {
                                if (detailPLCInfo.getType().equals(PLCWriteTypeEnum.LOW_SPEED_OPEN.toString())) {
                                    modbus4jWriteUtils.writeCoil(TcpMaster.getMaster(detailPLCInfo.getIp()), 1, detailPLCInfo.getOffset(), true);
                                } else if (detailPLCInfo.getType().equals(PLCWriteTypeEnum.LOW_SPEED_STOP.toString())) {
                                    modbus4jWriteUtils.writeCoil(TcpMaster.getMaster(detailPLCInfo.getIp()), 1, detailPLCInfo.getOffset(), false);
                                }
                            } else {
                                if (detailPLCInfo.getType().equals(PLCWriteTypeEnum.LOW_SPEED_OPEN.toString())) {
                                    modbus4jWriteUtils.writeCoil(TcpMaster.getMaster(detailPLCInfo.getIp()), 1, detailPLCInfo.getOffset(), false);
                                } else if (detailPLCInfo.getType().equals(PLCWriteTypeEnum.LOW_SPEED_STOP.toString())) {
                                    modbus4jWriteUtils.writeCoil(TcpMaster.getMaster(detailPLCInfo.getIp()), 1, detailPLCInfo.getOffset(), true);
                                }
                            }
                        } else {
                            if (isOpen) {
                                if (detailPLCInfo.getType().equals(PLCWriteTypeEnum.LOW_SPEED_OPEN.toString())) {
                                    modbus4jWriteUtils.writeCoil(TcpMaster.getMaster(detailPLCInfo.getIp()), 1, detailPLCInfo.getOffset(), true);
                                }
                            } else {
                                if (detailPLCInfo.getType().equals(PLCWriteTypeEnum.LOW_SPEED_OPEN.toString())) {
                                    modbus4jWriteUtils.writeCoil(TcpMaster.getMaster(detailPLCInfo.getIp()), 1, detailPLCInfo.getOffset(), false);
                                }
                            }
                        }
                    }
                }
            } else {
                for (Object temp : values) {
                    DetailPLCInfo detailPLCInfo = JSONObject.parseObject(temp.toString(), DetailPLCInfo.class);
                    if (detailPLCInfo.getIsRead().equals(0)) {
                        // isPulse == 1 表示为脉冲信号，需要两个指令控制
                        if (detailPLCInfo.getIsPulse().equals(1)) {
                            if (isOpen) {
                                if (detailPLCInfo.getType().equals(PLCWriteTypeEnum.OPEN.toString())) {
                                    modbus4jWriteUtils.writeCoil(TcpMaster.getMaster(detailPLCInfo.getIp()), 1, detailPLCInfo.getOffset(), true);
                                } else if (detailPLCInfo.getType().equals(PLCWriteTypeEnum.STOP.toString())) {
                                    modbus4jWriteUtils.writeCoil(TcpMaster.getMaster(detailPLCInfo.getIp()), 1, detailPLCInfo.getOffset(), false);
                                }
                            } else {
                                if (detailPLCInfo.getType().equals(PLCWriteTypeEnum.OPEN.toString())) {
                                    modbus4jWriteUtils.writeCoil(TcpMaster.getMaster(detailPLCInfo.getIp()), 1, detailPLCInfo.getOffset(), false);
                                } else if (detailPLCInfo.getType().equals(PLCWriteTypeEnum.STOP.toString())) {
                                    modbus4jWriteUtils.writeCoil(TcpMaster.getMaster(detailPLCInfo.getIp()), 1, detailPLCInfo.getOffset(), true);
                                }
                            }
                        } else {
                            if (isOpen) {
                                if (detailPLCInfo.getType().equals(PLCWriteTypeEnum.OPEN.toString())) {
                                    modbus4jWriteUtils.writeCoil(TcpMaster.getMaster(detailPLCInfo.getIp()), 1, detailPLCInfo.getOffset(), true);
                                }
                            } else {
                                if (detailPLCInfo.getType().equals(PLCWriteTypeEnum.OPEN.toString())) {
                                    modbus4jWriteUtils.writeCoil(TcpMaster.getMaster(detailPLCInfo.getIp()), 1, detailPLCInfo.getOffset(), false);
                                }
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        float valtage = (float) hex2decimal("0964");
        System.out.println(valtage);
        byte[] crcByte = new byte[]{0x01, 0x03, 0x00, 0x69, 0x00, 0x02};
        System.out.println(CRC.getCRC3(crcByte));
    }

    public static int hex2decimal(String hex) {
        return Integer.parseInt(hex, 16);
    }

}
