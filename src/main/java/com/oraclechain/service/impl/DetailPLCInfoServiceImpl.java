package com.oraclechain.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.oraclechain.config.RedisKeyConfig;
import com.oraclechain.dao.DetailPLCInfoDao;
import com.oraclechain.entity.DetailPLCInfo;
import com.oraclechain.entity.PLCReadTypeEnum;
import com.oraclechain.entity.PLCWriteTypeEnum;
import com.oraclechain.service.DetailPLCInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.List;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/7/24
 */
@Service
public class DetailPLCInfoServiceImpl implements DetailPLCInfoService {

    @Autowired
    private DetailPLCInfoDao detailPLCInfoDao;

    @Autowired
    private RedisTemplate<String, String> redisTemplate;

    @Override
    public List<DetailPLCInfo> select(DetailPLCInfo detailPLCInfo) {
        List<DetailPLCInfo> detailPLCInfoList = detailPLCInfoDao.select(detailPLCInfo);
        for (DetailPLCInfo plcInfo : detailPLCInfoList) {
            if (plcInfo.getIsRead().equals(1)) {
                plcInfo.setTypeName(PLCReadTypeEnum.valueOf(plcInfo.getType()).getName());
            } else {
                plcInfo.setTypeName(PLCWriteTypeEnum.valueOf(plcInfo.getType()).getName());
            }
        }
        return detailPLCInfoList;
    }

    @Override
    public void insert(DetailPLCInfo detailPLCInfo) {
        detailPLCInfoDao.insert(detailPLCInfo);
        redisTemplate.opsForHash().put(RedisKeyConfig.DEVICE_PLC_INFO + detailPLCInfo.getDeviceId(), detailPLCInfo.getPlcAddress(), JSONObject.toJSONString(detailPLCInfo));
        redisTemplate.opsForSet().add(RedisKeyConfig.DEVICE_PLC_IP_KEY + detailPLCInfo.getIp(), detailPLCInfo.getDeviceId().toString());
    }

    @Override
    public void update(DetailPLCInfo detailPLCInfo) {
        detailPLCInfoDao.update(detailPLCInfo);
        redisTemplate.opsForHash().put(RedisKeyConfig.DEVICE_PLC_INFO + detailPLCInfo.getDeviceId(), detailPLCInfo.getPlcAddress(), JSONObject.toJSONString(detailPLCInfo));
        redisTemplate.opsForSet().add(RedisKeyConfig.DEVICE_PLC_IP_KEY + detailPLCInfo.getIp(), detailPLCInfo.getDeviceId().toString());
    }

    @Override
    public void delete(Integer id) {
        DetailPLCInfo plcInfo = new DetailPLCInfo();
        plcInfo.setId(id);
        List<DetailPLCInfo> detailPLCInfos = detailPLCInfoDao.select(plcInfo);
        if (CollectionUtils.isEmpty(detailPLCInfos)) {
            return;
        }
        detailPLCInfoDao.delete(id);
        DetailPLCInfo detailPLCInfo = detailPLCInfos.get(0);
        redisTemplate.opsForHash().delete(RedisKeyConfig.DEVICE_PLC_INFO + detailPLCInfo.getDeviceId(), detailPLCInfo.getPlcAddress());
    }

    @Override
    public void updateCache() {
        try {
            List<DetailPLCInfo> detailPLCInfos = detailPLCInfoDao.select(new DetailPLCInfo());
            for (DetailPLCInfo plcInfo : detailPLCInfos) {
                redisTemplate.opsForHash().put(RedisKeyConfig.DEVICE_PLC_INFO + plcInfo.getDeviceId(), plcInfo.getPlcAddress(), JSONObject.toJSONString(plcInfo));
                redisTemplate.opsForSet().add(RedisKeyConfig.DEVICE_PLC_IP_KEY + plcInfo.getIp(), plcInfo.getDeviceId().toString());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
