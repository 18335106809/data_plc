package com.oraclechain.util;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/12/17
 */
public class CRC {
    public static String Make_CRC(byte[] data) {
        byte[] buf = new byte[data.length];// 存储需要产生校验码的数据
        for (int i = 0; i < data.length; i++) {
            buf[i] = data[i];
        }
        int len = buf.length;
        int crc = 0xFFFF;
        for (int pos = 0; pos < len; pos++) {
            if (buf[pos] < 0) {
                crc ^= (int) buf[pos] + 256; // XOR byte into least sig. byte of
                // crc
            } else {
                crc ^= (int) buf[pos]; // XOR byte into least sig. byte of crc
            }
            for (int i = 8; i != 0; i--) { // Loop over each bit
                if ((crc & 0x0001) != 0) { // If the LSB is set
                    crc >>= 1; // Shift right and XOR 0xA001
                    crc ^= 0xA001;
                } else
                    // Else LSB is not set
                    crc >>= 1; // Just shift right
            }
        }
        String c = Integer.toHexString(crc);
        if (c.length() == 4) {
            c = c.substring(2, 4) + c.substring(0, 2);
        } else if (c.length() == 3) {
            c = "0" + c;
            c = c.substring(2, 4) + c.substring(0, 2);
        } else if (c.length() == 2) {
            c = "0" + c.substring(1, 2) + "0" + c.substring(0, 1);
        }
        return c;
    }

//    01 06 00 10 00 03 C8 0E
//    01 06 00 10 00 00 88 0F
    public static void main(String[] args) {
        byte[] open = new byte[]{0x01, 0x06, 0x00, 0x10, 0x00, 0x03};
        byte[] close = new byte[]{0x01, 0x06, 0x00, 0x10, 0x00, 0x00};
        System.out.println(Make_CRC(open));
        System.out.println(Make_CRC(close));
    }
}
