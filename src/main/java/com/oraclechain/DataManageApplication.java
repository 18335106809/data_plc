package com.oraclechain;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/1/6
 */
@SpringBootApplication
public class DataManageApplication {
    public static void main(String[] args) {
        SpringApplication.run(DataManageApplication.class);
    }
}
