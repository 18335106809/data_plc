package com.oraclechain.config;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/1/14
 */
public class RedisKeyConfig {

    public static final String DEVICE_KEY = "device:";
    public static final String DEVICE_PARAMETER_KEY = "device.parameter:";
    public static final String DEVICE_SPACE_KEY= "spaceManage:";
    public static final String DEVICE_HEALTH_KEY = "device.health.";
    public static final String DEVICE_REALTIME_DATA = "device.real.time.data:";
    public static final String DEVICE_MOCK_KEY = "device.mock.";
    public static final String DEVICE_ALARM_KEY = "device.alarm:";
    public static final String DEVICE_PLC_INFO = "device.plc.info:";        // 用于某个设备的具体plcInfo
    public static final String DEVICE_PLC_IP_KEY= "device.plc.ip.key:";     // 用于某一ip下，都有哪些设备id
    public static final String DEVICE_TYPE_HALL_KEY = "device.type.hall.key:";
    public static final String DEVICE_TYPE_HALL_IP_KEY = "device.type.hall.ip.key:";
    public static final String DEVICE_SPECIAL_TYPE = "device.special.type";

}
