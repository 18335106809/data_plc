package com.oraclechain.entity;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/7/21
 */
public class PLCInfo {

    private Integer id;
    private String ip;
    private Integer port;
    private String type;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public Integer getPort() {
        return port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
