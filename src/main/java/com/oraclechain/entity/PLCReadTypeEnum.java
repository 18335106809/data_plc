package com.oraclechain.entity;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/9/23
 */
public enum PLCReadTypeEnum {
    CUTOVER("本地/远程切换"),
    RUN("运行"),
    TURN_ON("打开"),
    CLOSE("关闭"),
    BREAKDOWN("故障"),
    LOW_SPEED_RUN("低速运行"),
    HIGH_SPEED_RUN("高速运行"),
    LOW_SPEED_BREAKDOWN("低速故障"),
    HIGH_SPEED_BREAKDOWN("高速故障"),
    AUTO_FEEDBACK("自动信号反馈"),
    LOCK_POSITION("锁位指示"),
    LOCK_LEVER("锁杆状态"),
    LOCK_BOLT("锁舌状态"),
    INCLINATION("倾角状态"),
    START_PUMP("启泵水位"),
    STOP_PUMP("停泵水位"),
    HIGH_LEVEL_START("高液位—启"),
    LOW_LEVEL_STOP("低液位—停"),
    VALUE("探测器值");


    private String name;

    PLCReadTypeEnum(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static JSONObject getValues() {
        JSONObject result = new JSONObject();
        for (PLCReadTypeEnum plcReadTypeEnum : PLCReadTypeEnum.values()) {
            result.put(plcReadTypeEnum.toString(), plcReadTypeEnum.getName());
        }
        return result;
    }

    public static PLCReadTypeEnum getName(String name) {
        for (PLCReadTypeEnum plcReadTypeEnum : PLCReadTypeEnum.values()) {
            if (plcReadTypeEnum.name.equals(name)) {
                return plcReadTypeEnum;
            }
        }
        return null;
    }
}
