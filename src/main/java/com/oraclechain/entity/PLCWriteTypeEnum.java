package com.oraclechain.entity;

import com.alibaba.fastjson.JSONObject;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/9/23
 */
public enum PLCWriteTypeEnum {
    OPEN("启动"),
    STOP("停止"),
    LOW_SPEED_OPEN("低速启动"),
    HIGH_SPEED_OPEN("高速启动"),
    LOW_SPEED_STOP("低速停止"),
    HIGH_SPEED_STOP("高速停止"),
    AUTO("自动信号");

    private String name;

    PLCWriteTypeEnum(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static JSONObject getValues() {
        JSONObject result = new JSONObject();
        for (PLCWriteTypeEnum plcWriteTypeEnum : PLCWriteTypeEnum.values()) {
            result.put(plcWriteTypeEnum.toString(), plcWriteTypeEnum.getName());
        }
        return result;
    }

    public static PLCWriteTypeEnum getName(String name) {
        for (PLCWriteTypeEnum plcWriteTypeEnum : PLCWriteTypeEnum.values()) {
            if (plcWriteTypeEnum.name.equals(name)) {
                return plcWriteTypeEnum;
            }
        }
        return null;
    }
}
