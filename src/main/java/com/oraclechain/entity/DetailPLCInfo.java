package com.oraclechain.entity;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/7/21
 */
public class DetailPLCInfo {

    private Integer id;             // id
    private String ip;              // ip
    private Integer port;           // 端口
    private Long deviceId;          // 设备ID
    private String type;            // plc类型，具体说明见typeName
    private String plcAddress;      // plc上地址
    private Integer offset;         // 偏移量
    private Integer isRead;         // 是否为读，1为读，0为写
    private Integer isPulse;        // 是否为脉冲信号，1为脉冲信号，0为持续信号
    private String dataKey;         // 所对应的数据Key，eg:该信号负责读取温度，则对应template
    private String description;     // 对plc的说明
    private Integer readType;       // 有4个值,分别为[1,2,3,4]。此项目中只用到1、3。
                                    // 1.读线圈、2.读离散输入、3.读保存寄存器、4.读输入寄存器

    private Object value;
    private String typeName;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public Integer getPort() {
        return port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }

    public Long getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(Long deviceId) {
        this.deviceId = deviceId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPlcAddress() {
        return plcAddress;
    }

    public void setPlcAddress(String plcAddress) {
        this.plcAddress = plcAddress;
    }

    public Integer getIsRead() {
        return isRead;
    }

    public void setIsRead(Integer isRead) {
        this.isRead = isRead;
    }

    public Integer getIsPulse() {
        return isPulse;
    }

    public void setIsPulse(Integer isPulse) {
        this.isPulse = isPulse;
    }

    public String getDataKey() {
        return dataKey;
    }

    public void setDataKey(String dataKey) {
        this.dataKey = dataKey;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    public Integer getOffset() {
        return offset;
    }

    public void setOffset(Integer offset) {
        this.offset = offset;
    }

    public Integer getReadType() {
        return readType;
    }

    public void setReadType(Integer readType) {
        this.readType = readType;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }
}
