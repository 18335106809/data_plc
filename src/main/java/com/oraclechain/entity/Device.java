package com.oraclechain.entity;

import com.alibaba.fastjson.JSONObject;

import java.util.Date;

public class Device {

    private Long id;
    /**
     * 设备名称
     */
    private String name;
    /**
     * 设备型号
     */
    private String unitType;

    /**
     * 生产日期
     */
    private Date producedTime;

    /**
     * 保修期
     */
    private String warranty;

    /**
     * 设备类型
     */
    private String type;

    /**
     * 所在舱体
     */
    private Long capsule;
    /**
     * 所在舱体组
     */
    private String capsuleGroup;
    /**
     * 设备位置
     */
    private String capsuleLocation;

    /**
     * 设备协议
     */
    private String protocol;

    /**
     * 设备状态
     */
    private Integer status;

    /**
     * 设备参数
     */
    private String parameters;


    /**
     * 健康值
     */
    private String healthIndex;


    /**
     * 设备阈值
     */
    private String threshold;

    /**
     * 所属PLCname
     */
    private String PlcName;

    /**
     * 所属PLCname
     */
    private String overhaul;

    private String theory;

    private String vendorName;

    private String code;

    private Date createTime;

    private Date updateTime;


    private Object value;

    private Long gatherTime;

    private String position;

    private String iconfont;

    private String color;

    private String spaceName;

    private String capsuleName;

    private String plcInfo;

    private DetailPLCInfo plcInfoJson;

    private String orderDataKey;

    public Device() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUnitType() {
        return unitType;
    }

    public void setUnitType(String unitType) {
        this.unitType = unitType;
    }

    public Date getProducedTime() {
        return producedTime;
    }

    public void setProducedTime(Date producedTime) {
        this.producedTime = producedTime;
    }

    public String getWarranty() {
        return warranty;
    }

    public void setWarranty(String warranty) {
        this.warranty = warranty;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Long getCapsule() {
        return capsule;
    }

    public void setCapsule(Long capsule) {
        this.capsule = capsule;
    }

    public String getCapsuleGroup() {
        return capsuleGroup;
    }

    public void setCapsuleGroup(String capsuleGroup) {
        this.capsuleGroup = capsuleGroup;
    }

    public String getCapsuleLocation() {
        return capsuleLocation;
    }

    public void setCapsuleLocation(String capsuleLocation) {
        this.capsuleLocation = capsuleLocation;
    }

    public String getProtocol() {
        return protocol;
    }

    public void setProtocol(String protocol) {
        this.protocol = protocol;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getParameters() {
        return parameters;
    }

    public void setParameters(String parameters) {
        this.parameters = parameters;
    }

    public String getHealthIndex() {
        return healthIndex;
    }

    public void setHealthIndex(String healthIndex) {
        this.healthIndex = healthIndex;
    }

    public String getThreshold() {
        return threshold;
    }

    public void setThreshold(String threshold) {
        this.threshold = threshold;
    }

    public String getPlcName() {
        return PlcName;
    }

    public void setPlcName(String plcName) {
        PlcName = plcName;
    }

    public String getOverhaul() {
        return overhaul;
    }

    public void setOverhaul(String overhaul) {
        this.overhaul = overhaul;
    }

    public String getTheory() {
        return theory;
    }

    public void setTheory(String theory) {
        this.theory = theory;
    }

    public String getVendorName() {
        return vendorName;
    }

    public void setVendorName(String vendorName) {
        this.vendorName = vendorName;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    public Long getGatherTime() {
        return gatherTime;
    }

    public void setGatherTime(Long gatherTime) {
        this.gatherTime = gatherTime;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getIconfont() {
        return iconfont;
    }

    public void setIconfont(String iconfont) {
        this.iconfont = iconfont;
    }

    public String getCapsuleName() {
        return capsuleName;
    }

    public void setCapsuleName(String capsuleName) {
        this.capsuleName = capsuleName;
    }

    public String getSpaceName() {
        return spaceName;
    }

    public void setSpaceName(String spaceName) {
        this.spaceName = spaceName;
    }

    public String getPlcInfo() {
        return plcInfo;
    }

    public void setPlcInfo(String plcInfo) {
        this.plcInfo = plcInfo;
    }

    public DetailPLCInfo getPlcInfoJson() {
        return plcInfoJson;
    }

    public void setPlcInfoJson(DetailPLCInfo plcInfoJson) {
        this.plcInfoJson = plcInfoJson;
    }

    public String getOrderDataKey() {
        return orderDataKey;
    }

    public void setOrderDataKey(String orderDataKey) {
        this.orderDataKey = orderDataKey;
    }
}
