package com.oraclechain.entity;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/12/21
 */
public class TriggerAlarm {
    private Boolean isOpen;

    public TriggerAlarm(Boolean isOpen) {
        this.isOpen = isOpen;
    }

    public Boolean getOpen() {
        return isOpen;
    }

    public void setOpen(Boolean open) {
        isOpen = open;
    }
}
