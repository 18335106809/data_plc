package com.oraclechain.entity;

import com.alibaba.fastjson.JSONObject;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/4/10
 */
public class DeviceData {

    private Long id;
    private JSONObject param;
    private Boolean isMock = false;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public JSONObject getParam() {
        return param;
    }

    public void setParam(JSONObject param) {
        this.param = param;
    }

    public Boolean getMock() {
        return isMock;
    }

    public void setMock(Boolean mock) {
        isMock = mock;
    }
}
