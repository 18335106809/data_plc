package com.oraclechain.udp;

import com.oraclechain.config.RedisKeyConfig;
import com.oraclechain.entity.TriggerAlarm;
import io.netty.bootstrap.Bootstrap;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.DatagramChannel;
import io.netty.channel.socket.DatagramPacket;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioDatagramChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.net.InetSocketAddress;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author liuhualong
 * @Description:        A相电压
 * @date 2020/10/15
 */
@Component
@EnableScheduling
public class NettyUdpClient {

    static Logger logger = LoggerFactory.getLogger(NettyUdpClient.class);

    @Autowired
    private RedisTemplate<String, String> redisTemplate;

    @Autowired
    private UdpDataHandler udpHandler;

    @Autowired
    private ClientHandler clientHandler;

//    static {
//        udpHandler = SpringContextUtil.getBean(UdpHandler.class);
//    }


    @Value("${udp.client.port.num}")
    private Integer portNum;

    @Value("${udp.server.port}")
    private Integer serverPort;

    @Value("${udp.client.port.start}")
    private Integer portStart;

    @Value("${device.type.hall.data}")
    private String ipSet;

    @Value("${device.hall.ip.t}")
    private String ip_t;

    @Value("${device.trigger.alarm.ip}")
    private String alarmIp;

    @Value("${device.trigger.alarm.port}")
    private Integer alarmPort;

    private static Map<Integer, Channel> channelMap = new ConcurrentHashMap<>();
    private static Map<Channel, Integer> channelIntegerMap = new ConcurrentHashMap<>();
    private static Channel channelAlarm;

    final static byte[] AV = new byte[]{0x00, 0x03, 0x00, 0x49, 0x00, 0x01, (byte) 0x54, 0x0D};    // A压
    final static byte[] BV = new byte[]{0x00, 0x03, 0x00, 0x4A, 0x00, 0x01, (byte) 0xA4, 0x0D};    // B压
    final static byte[] CV = new byte[]{0x00, 0x03, 0x00, 0x4B, 0x00, 0x01, (byte) 0xF5, (byte) 0xCD};    // C压
    final static byte[] AI = new byte[]{0x00, 0x03, 0x00, 0x4C, 0x00, 0x01, 0x44, 0x0C};    // A流
    final static byte[] BI = new byte[]{0x00, 0x03, 0x00, 0x4D, 0x00, 0x01, 0x15, (byte) 0xCC};    // B流
    final static byte[] CI = new byte[]{0x00, 0x03, 0x00, 0x4E, 0x00, 0x01, (byte) 0xE5, (byte) 0xCC};    // C流
    final static byte[] AE = new byte[]{0x00, 0x03, 0x00, 0x63, 0x00, 0x02, 0x35, (byte) 0xC4};    // 有功电能
    final static byte[] RE = new byte[]{0x00, 0x03, 0x00, 0x69, 0x00, 0x02, 0x15, (byte) 0xC6};    // 无功电能

    final static byte[] AV_T = new byte[]{0x01, 0x03, 0x00, 0x49, 0x00, 0x01, (byte) 0x55, (byte) 0xDC};    // A压
    final static byte[] BV_T = new byte[]{0x01, 0x03, 0x00, 0x4A, 0x00, 0x01, (byte) 0xA5, (byte) 0xDC};    // B压
    final static byte[] CV_T = new byte[]{0x01, 0x03, 0x00, 0x4B, 0x00, 0x01, (byte) 0xF4, (byte) 0x1C};    // C压
    final static byte[] AI_T = new byte[]{0x01, 0x03, 0x00, 0x4C, 0x00, 0x01, 0x45, (byte) 0xDD};    // A流
    final static byte[] BI_T = new byte[]{0x01, 0x03, 0x00, 0x4D, 0x00, 0x01, 0x14, (byte) 0x1D};    // B流
    final static byte[] CI_T = new byte[]{0x01, 0x03, 0x00, 0x4E, 0x00, 0x01, (byte) 0xE4, (byte) 0x1D};    // C流
    final static byte[] AE_T = new byte[]{0x01, 0x03, 0x00, 0x63, 0x00, 0x02, 0x34, (byte) 0x15};    // 有功电能
    final static byte[] RE_T = new byte[]{0x01, 0x03, 0x00, 0x69, 0x00, 0x02, 0x14, (byte) 0x17};    // 无功电能

    final static byte[] open = new byte[]{0x01, 0x06, 0x00, 0x10, 0x00, 0x03, (byte) 0xC8, 0x0E};
    final static byte[] close = new byte[]{0x01, 0x06, 0x00, 0x10, 0x00, 0x00, 88, 0x0F};

//    @PostConstruct
    public void start() throws InterruptedException {
        for (int i = 1; i <= portNum; i++) {
            Integer port = Integer.valueOf(portStart.toString() + i);
            Bootstrap bootstrap = new Bootstrap();
            EventLoopGroup work = new NioEventLoopGroup();
            bootstrap.group(work)
                    .channel(NioDatagramChannel.class)
                    .option(ChannelOption.SO_BROADCAST, true)
                    .handler(new ChannelInitializer<DatagramChannel>() {
                        @Override
                        protected void initChannel(DatagramChannel socketChannel) throws Exception {
                            ByteBuf delimiter = Unpooled.copiedBuffer(new byte[]{(byte) 0xDD});
                            socketChannel.pipeline().addLast(udpHandler);
                        }
                    });
            ChannelFuture future = bootstrap.bind(port).sync();
            Channel channel = future.channel();
            if (future.isSuccess()){
                channelMap.put(i, channel);
                channelIntegerMap.put(channel, port);
                logger.info("udp client start up");
            }
        }

        EventLoopGroup group = new NioEventLoopGroup();
        try{
            Bootstrap clientBootstrap = new Bootstrap();

            clientBootstrap.group(group);
            clientBootstrap.channel(NioSocketChannel.class);
            clientBootstrap.remoteAddress(new InetSocketAddress(alarmIp, alarmPort));
            clientBootstrap.handler(new ChannelInitializer<SocketChannel>() {
                protected void initChannel(SocketChannel socketChannel) throws Exception {
                    socketChannel.pipeline().addLast(new ClientHandler());
                }
            });
            ChannelFuture channelFuture = clientBootstrap.connect().sync();
            channelFuture.channel().closeFuture().sync();
            if (channelFuture.isSuccess()) {
                channelAlarm = channelFuture.channel();
                logger.info("trigger alarm client start up");
            }
        } finally {
            group.shutdownGracefully().sync();
        }
    }

    public static void sendAlarm(TriggerAlarm triggerAlarm) {
        if (triggerAlarm.getOpen()) {
            channelAlarm.writeAndFlush(open);
        } else {
            channelAlarm.writeAndFlush(close);
        }
    }

//    @Scheduled(cron = "0 0 0/1 * * ?")
    public void mockData() {
        try {
            Set<String> ips = redisTemplate.opsForSet().members(RedisKeyConfig.DEVICE_TYPE_HALL_KEY + ipSet);
            Integer port;
            Channel channel;
            for (String ip : ips) {
                for (Map.Entry<Integer, Channel> entry : channelMap.entrySet()) {
                    port = entry.getKey();
                    channel = entry.getValue();
                    if (ip.equals(ip_t)) {
                        switch (port) {
                            case 1:
                                channel.writeAndFlush(new DatagramPacket(Unpooled.copiedBuffer(AV_T),
                                        new InetSocketAddress(ip, serverPort))).sync();
                                Thread.sleep(300);
                                break;
                            case 2:
                                channel.writeAndFlush(new DatagramPacket(Unpooled.copiedBuffer(BV_T),
                                        new InetSocketAddress(ip, serverPort))).sync();
                                Thread.sleep(300);
                                break;
                            case 3:
                                channel.writeAndFlush(new DatagramPacket(Unpooled.copiedBuffer(CV_T),
                                        new InetSocketAddress(ip, serverPort))).sync();
                                Thread.sleep(500);
                                break;
                            case 4:
                                channel.writeAndFlush(new DatagramPacket(Unpooled.copiedBuffer(AI_T),
                                        new InetSocketAddress(ip, serverPort))).sync();
                                Thread.sleep(300);
                                break;
                            case 5:
                                channel.writeAndFlush(new DatagramPacket(Unpooled.copiedBuffer(BI_T),
                                        new InetSocketAddress(ip, serverPort))).sync();
                                Thread.sleep(300);
                                break;
                            case 6:
                                channel.writeAndFlush(new DatagramPacket(Unpooled.copiedBuffer(CI_T),
                                        new InetSocketAddress(ip, serverPort))).sync();
                                Thread.sleep(300);
                                break;
                            case 7:
                                channel.writeAndFlush(new DatagramPacket(Unpooled.copiedBuffer(AE_T),
                                        new InetSocketAddress(ip, serverPort))).sync();
                                Thread.sleep(300);
                                break;
                            case 8:
                                channel.writeAndFlush(new DatagramPacket(Unpooled.copiedBuffer(RE_T),
                                        new InetSocketAddress(ip, serverPort))).sync();
                                Thread.sleep(300);
                                break;
                        }
                    } else {
                        switch (port) {
                            case 1:
                                channel.writeAndFlush(new DatagramPacket(Unpooled.copiedBuffer(AV),
                                        new InetSocketAddress(ip, serverPort))).sync();
                                Thread.sleep(300);
                                break;
                            case 2:
                                channel.writeAndFlush(new DatagramPacket(Unpooled.copiedBuffer(BV),
                                        new InetSocketAddress(ip, serverPort))).sync();
                                Thread.sleep(300);
                                break;
                            case 3:
                                channel.writeAndFlush(new DatagramPacket(Unpooled.copiedBuffer(CV),
                                        new InetSocketAddress(ip, serverPort))).sync();
                                Thread.sleep(500);
                                break;
                            case 4:
                                channel.writeAndFlush(new DatagramPacket(Unpooled.copiedBuffer(AI),
                                        new InetSocketAddress(ip, serverPort))).sync();
                                Thread.sleep(300);
                                break;
                            case 5:
                                channel.writeAndFlush(new DatagramPacket(Unpooled.copiedBuffer(BI),
                                        new InetSocketAddress(ip, serverPort))).sync();
                                Thread.sleep(300);
                                break;
                            case 6:
                                channel.writeAndFlush(new DatagramPacket(Unpooled.copiedBuffer(CI),
                                        new InetSocketAddress(ip, serverPort))).sync();
                                Thread.sleep(300);
                                break;
                            case 7:
                                channel.writeAndFlush(new DatagramPacket(Unpooled.copiedBuffer(AE),
                                        new InetSocketAddress(ip, serverPort))).sync();
                                Thread.sleep(300);
                                break;
                            case 8:
                                channel.writeAndFlush(new DatagramPacket(Unpooled.copiedBuffer(RE),
                                        new InetSocketAddress(ip, serverPort))).sync();
                                Thread.sleep(300);
                                break;
                        }
                    }
                }
            }
            logger.info("send success");
        } catch (Exception e) {
            logger.error("定时读取电表数据出错");
            e.printStackTrace();
        }
    }

    public static Integer getIndex(Channel channel) {
        if (channelIntegerMap.containsKey(channel)) {
            return channelIntegerMap.get(channel);
        } else {
            return null;
        }
    }

}
