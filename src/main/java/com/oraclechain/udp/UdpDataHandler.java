package com.oraclechain.udp;

import com.alibaba.fastjson.JSONObject;
import com.oraclechain.config.RedisKeyConfig;
import com.oraclechain.entity.Device;
import com.oraclechain.entity.DeviceData;
import com.oraclechain.mq.TopicRabbitConfig;
import com.oraclechain.util.HexStringUtil;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.channel.socket.DatagramPacket;
import io.netty.util.internal.StringUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.net.InetSocketAddress;


/**
 * @author liuhualong
 * @Description:
 * @date 2020/10/19
 */
@Component
@ChannelHandler.Sharable
public class UdpDataHandler extends SimpleChannelInboundHandler<DatagramPacket> {

    static Logger logger = LoggerFactory.getLogger(UdpDataHandler.class);

//    private static RedisTemplate<String, String> redisTemplate;
    @Autowired
    private RedisTemplate<String, String> redisTemplate;

    @Autowired
    private RabbitTemplate rabbitTemplate;

//    static {
//        redisTemplate = SpringContextUtil.getBean("stringRedisTemplate", RedisTemplate.class);
//    }

    @Value("${udp.client.port.start}")
    private Integer portStart;

    @Value("${udp.server.port}")
    private Integer serverPort;

    @Value("${device.type.hall.AV}")
    private String av;

    @Value("${device.type.hall.BV}")
    private String bv;

    @Value("${device.type.hall.CV}")
    private String cv;

    @Value("${device.type.hall.AI}")
    private String ai;

    @Value("${device.type.hall.BI}")
    private String bi;

    @Value("${device.type.hall.CI}")
    private String ci;

    @Value("${device.type.hall.AE}")
    private String ae;

    @Value("${device.type.hall.RE}")
    private String re;

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, DatagramPacket msg) throws Exception {
        try {
            InetSocketAddress sender = msg.sender();
            String ip = sender.getAddress().toString().substring(1);
            Integer port = NettyUdpClient.getIndex(ctx.channel());
            if (port == null) {
                return;
            }
//            Integer port = sender.getPort();
            String index = port.toString().replace(portStart.toString(), "");
            logger.info("ip : " + ip + " port : " + sender.getPort());
            String deviceId = redisTemplate.opsForValue().get(RedisKeyConfig.DEVICE_TYPE_HALL_IP_KEY + ip);
            Device device = getDevice(deviceId);
            if (device == null) {
                return;
            }
            DeviceData deviceData = new DeviceData();
            deviceData.setId(device.getId());
            JSONObject jsonObject = new JSONObject();
            ByteBuf content = msg.content();
            byte[] req = new byte[content.readableBytes()];
            content.readBytes(req);
//            char[] chars = HexStringUtil.encodeHex(req, false);
            String data = HexStringUtil.encodeHexStr(req, false);
            logger.info("data : " + data);
//            String data = chars.toString();
            Float result;
            switch (Integer.valueOf(index)) {
                case 1:
                    result = getNum(ctx, ip, data, NettyUdpClient.AV, 0.1F, 14, "0302");
                    if (result != null) {
                        jsonObject.put(av, result);
                    }
                    break;
                case 2:
                    result = getNum(ctx, ip, data, NettyUdpClient.BV, 0.1F, 14, "0302");
                    if (result != null) {
                        jsonObject.put(bv, result);
                    }
                    break;
                case 3:
                    result = getNum(ctx, ip, data, NettyUdpClient.CV, 0.1F, 14, "0302");
                    if (result != null) {
                        jsonObject.put(cv, result);
                    }
                    break;
                case 4:
                    result = getNum(ctx, ip, data, NettyUdpClient.AI, 0.01F, 14, "0302");
                    if (result != null) {
                        jsonObject.put(ai, result);
                    }
                    break;
                case 5:
                    result = getNum(ctx, ip, data, NettyUdpClient.BI, 0.01F, 14, "0302");
                    if (result != null) {
                        jsonObject.put(bi, result);
                    }
                    break;
                case 6:
                    result = getNum(ctx, ip, data, NettyUdpClient.CI, 0.01F, 14, "0302");
                    if (result != null) {
                        jsonObject.put(ci, result);
                    }
                    break;
                case 7:
                    result = getNum(ctx, ip, data, NettyUdpClient.AE, 0.01F, 18, "0304");
                    if (result != null) {
                        jsonObject.put(ae, result);
                    }
                    break;
                case 8:
                    result = getNum(ctx, ip, data, NettyUdpClient.RE, 0.01F, 18, "0304");
                    if (result != null) {
                        jsonObject.put(re, result);
                    }
                    break;
            }
            if (!jsonObject.isEmpty()) {
                deviceData.setParam(jsonObject);
                this.rabbitTemplate.convertAndSend(TopicRabbitConfig.EXCHANGE, TopicRabbitConfig.DEVICE, JSONObject.toJSONString(deviceData));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Float getNum(ChannelHandlerContext context, String ip, String data, byte[] bytes, float rate, Integer length, String indexStr) {
        if (!StringUtils.isEmpty(data) && length.equals(data.length()) && data.indexOf(indexStr) == 2) {
            String hex = StringUtil.EMPTY_STRING;
            if (indexStr.equals("0302")) {
                hex = data.substring(6, 10);
            } else if (indexStr.equals("0304")) {
                hex = data.substring(6, 14);
            }
            float valtage = (float) hex2decimal(hex);
            return valtage * rate;
        } else {
            context.writeAndFlush(new DatagramPacket(Unpooled.copiedBuffer(bytes),
                    new InetSocketAddress(ip, serverPort)));
            return null;
        }
    }

    public Device getDevice(String deviceId) {
        String value = redisTemplate.opsForValue().get(RedisKeyConfig.DEVICE_KEY + deviceId);
        if (StringUtils.isEmpty(value)) {
            return null;
        }
        Device device = JSONObject.parseObject(value, Device.class);
        return device;
    }

    public static int hex2decimal(String hex) {
        return Integer.parseInt(hex, 16);
    }

    public static void main(String[] args) {
        String str = "0003020018422D";
        System.out.println(str.indexOf("0302"));
        System.out.println(str.substring(6, 10));
    }
}
