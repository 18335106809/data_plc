package com.oraclechain.mq;

import com.alibaba.fastjson.JSONObject;
import com.oraclechain.entity.Device;
import com.oraclechain.entity.TriggerAlarm;
import com.oraclechain.service.CommandService;
import com.oraclechain.udp.NettyUdpClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/7/20
 */
@Component
public class RabbitReceive {

    static Logger logger = LoggerFactory.getLogger(RabbitReceive.class);

    @Autowired
    private CommandService commandService;

    @RabbitListener(queues = TopicRabbitConfig.DEVICE_OPEN)
    public void handleMessageOpen(Message message) {
        try {
            logger.info("接收下发指令的消息，{{open device}}");
            Device deviceData = JSONObject.parseObject(new String(message.getBody()), Device.class);
            commandService.sendOpenCommand(deviceData);
            // TODO: 2020/7/20  根据设备信息下发相关指令
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("下发打开设备开关的指令出错");
        }
    }

    @RabbitListener(queues = TopicRabbitConfig.DEVICE_CLOSE)
    public void handleMessageClose(Message message) {
        try {
            logger.info("接收下发指令的消息，{{close device}}");
            Device deviceData = JSONObject.parseObject(new String(message.getBody()), Device.class);
            commandService.sendCloseCommand(deviceData);
            // TODO: 2020/7/20  根据设备信息下发相关指令
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("下发关闭设备开关的指令出错");
        }
    }

    @RabbitListener(queues = TopicRabbitConfig.DEVICE_ALARM)
    public void handleMessageAlarm(Message message) {
        try {
            logger.info("接收下发聲光報警器開關指令的消息：" + message.getBody());
            TriggerAlarm alarm = JSONObject.parseObject(new String(message.getBody()), TriggerAlarm.class);
            NettyUdpClient.sendAlarm(alarm);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("下发聲光報警器开关的指令出错");
        }
    }
}
