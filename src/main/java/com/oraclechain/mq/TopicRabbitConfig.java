package com.oraclechain.mq;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author liuhualong
 * @Description:
 * @date 2020/1/6
 */
@Configuration
public class TopicRabbitConfig {

    public final static String EXCHANGE = "topicExchange";      // 消费消息交换机

    public final static String EXCHANGE_OUT = "exchange_out";   // 生产消息交换机

    public final static String EXCHANGE_SPECIAL = "exchange_special";  // 特殊设备交换机

    public final static String EXCHANGE_ALARM = "exchange_alarm";  // 特殊设备交换机

    //绑定topic
    public final static String DEVICE = "device";               // 接收消息路由

    //下发硬件指令 topic
    public final static String DEVICE_OPEN = "device_open";   // 生产消息路由

    //下发硬件指令 topic
    public final static String DEVICE_CLOSE = "device_close";   // 生产消息路由

    public final static String DEVICE_SPECIAL = "device_special";  // 特殊设备

    public final static String DEVICE_ALARM= "device_alarm";  // 報警設備

    @Bean
    public Queue deviceQueue() {
        return new Queue(DEVICE);
    }

    @Bean
    public Queue deviceOpenQueue() {
        return new Queue(DEVICE_OPEN);
    }

    @Bean
    public Queue deviceCloseQueue() {
        return new Queue(DEVICE_CLOSE);
    }

    @Bean
    public Queue deviceSpecialQueue() {
        return new Queue(DEVICE_SPECIAL);
    }

    @Bean
    public Queue deviceAlarmQueue() {
        return new Queue(DEVICE_ALARM);
    }

    @Bean
    TopicExchange exchange() {
        return new TopicExchange(EXCHANGE);
    }

    @Bean
    TopicExchange exchangeOut() {
        return new TopicExchange(EXCHANGE_OUT);
    }

    @Bean
    TopicExchange exchangeSpecial() {
        return new TopicExchange(EXCHANGE_SPECIAL);
    }

    @Bean
    TopicExchange exchangeAlarm() {
        return new TopicExchange(EXCHANGE_ALARM);
    }

    @Bean
    Binding bindingExchangeMessage() {
        return BindingBuilder.bind(deviceQueue()).to(exchange()).with(DEVICE);
    }

    @Bean
    Binding bindingExchangeOutOpenMessage() {
        return BindingBuilder.bind(deviceOpenQueue()).to(exchangeOut()).with(DEVICE_OPEN);
    }

    @Bean
    Binding bindingExchangeOutCloseMessage() {
        return BindingBuilder.bind(deviceCloseQueue()).to(exchangeOut()).with(DEVICE_CLOSE);
    }

    @Bean
    Binding bindingExchangeSpecialCloseMessage() {
        return BindingBuilder.bind(deviceSpecialQueue()).to(exchangeSpecial()).with(DEVICE_SPECIAL);
    }


    @Bean
    Binding bindingExchangeAlarmMessage() {
        return BindingBuilder.bind(deviceAlarmQueue()).to(exchangeAlarm()).with(DEVICE_ALARM);
    }


}
